#!/usr/bin/python3

import sys
import os
import requests
import json
import subprocess
import urllib3
import base64
import syslog
import copy
import pdb


#####################################################
#
#  Class:  Protect_Endpoint
#
#  Provides methods to access endpoints
#
#####################################################
class Protect_Endpoint:

    #############################################
    #
    #  Constructor
    #
    #############################################
    def __init__(self, cookie, auth, ip, session):

        self.LOG_TAG=self.__class__.__name__
        self.deviceName = ""
        self.cookie = cookie
        self.auth = auth
        self.ip = ip
        self.session = session
        self.endpoint = ""
        self.RESTpwd = ""
        self.SFTPpwd = ""
        self.secretsList = ""
        self.sshKey = ""

        # Save the REST and SFTP passwords for all devices. To be used later if passwords are to be updated.

        secrets = json.loads(subprocess.check_output(
                "kubectl get secret devices-secret -o json", shell=True))['data']['.secret-file']
        self.secretslist = base64.b64decode(secrets).decode('utf-8').split()


    ####################################################
    #
    #  Method: getEndpoint
    #
    #  Returns:  Request status  AND
    #            if successful, details in JSON format
    #            else empty string
    #
    ####################################################
    def getEndpoint(self, deviceName):

        status = -1

        ##  sshKey is returned in this request.

        self.getAllEndpointTypes()

        ##  Get json with endpoint current configuration

        endpointURL = 'https://'+self.ip+'/api/service/dig/config/endpoint/'+deviceName
        ep = requests.get(endpointURL, cookies=self.cookie, headers=self.auth, verify=False)
        if ep.status_code == 200:
            status = 0
            self.deviceName = deviceName
            self.endpoint = json.loads(ep.text)

            # Save the REST and SFTP password for this device

            self.SFTPpassword = ""
            self.RESTpassword = ""
            for i in self.secretslist:
                if i.split(':')[0] == self.deviceName:
                    if i.split(':')[2] == 'CHANNEL_TYPE_SFTP':
                        self.SFTPpwd = i.split(':')[3]
                    elif i.split(':')[2] == 'CHANNEL_TYPE_REST':
                        self.RESTpwd = i.split(':')[3]

            return status, ep
        else:
            return status, ""

    ####################################################
    #
    #  Method: getAllEndpoints
    #
    #  Returns:  Request status  AND
    #            if successful, details in JSON format
    #            else empty string
    #
    ####################################################
    def getAllEndpoints(self):

        ##  sshKey is returned in this request. 

        self.getAllEndpointTypes()

        ##  Get all device endpoints

        endpointURL = 'https://'+self.ip+'/api/service/dig/config/endpoint'
        endpoints = requests.get(endpointURL, cookies=self.cookie, headers=self.auth, verify=False)
        if endpoints.status_code == 200:
            return endpoints.status_code, json.loads(endpoints.text)['endPoints']
        else:
            return endpoints.status_code, ""

    
    ####################################################
    #
    #  Method: getSSHkey
    #
    #  Returns:  Request status  AND
    #            if successful, details in JSON format
    #            else empty string
    #
    ####################################################
    def getSSHkey(self):
        return(self.sshKey)

    ####################################################
    #
    #  Method: getAllEndpointTypes
    #
    #  Returns:  Request status  AND
    #            if successful, details in JSON format
    #            else empty string
    #
    ####################################################
    def getAllEndpointTypes(self):

        ##  Get all endpoint types

        epTypeURL = 'https://'+self.ip+'/api/service/dig/config/'
        epTypes = requests.get(epTypeURL, cookies=self.cookie, headers=self.auth, verify=False)

        if epTypes.status_code == 200:
            self.sshKey = json.loads(epTypes.text)['sshKey']
            return epTypes.status_code, json.loads(epTypes.text)
        else:
            return epTypes.status_code, ""

    ####################################################
    #
    #  Method: getSFTPpassword
    #
    #  Returns:  Return the channel's SFTP password
    #
    ####################################################
    def getSFTPpassword(self):
        SFTPpassword = ""

        ##  If endpoint has not been retrieved, return empty password
        if self.endpoint == "":
            return ""

        return self.SFTPpwd


    ####################################################
    #
    #  Method: getRESTpassword
    #
    #  Returns:  Return the channel's REST password
    #
    ####################################################
    def getRESTpassword(self):
        RESTpassword = ""

        ##  If endpoint has not been retrieved, return empty password
        if self.endpoint == "":
            return ""

        return self.RESTpwd

    #######################################################################
    #
    #  Method: getEndpointChannels
    #
    #  Returns:  Returns array of JSON objects, one for each channel 
    #            in the endpoint.
    #  JSON keys:   type, remoteHost, authType
    #
    #######################################################################
    def getEndpointChannels(self):

        channels = []
        for ch in range(len(self.endpoint['channels'])):
            chl = self.endpoint['channels'][ch]
            channels.append({'type':chl['type'], 'remoteHost':chl['remoteHost'], 'authType':chl['authType']})

        return channels

    #######################################################################
    #
    #  Method: getEndpointStreams
    #
    #  Returns:  Returns array of JSON objects, one for each stream 
    #            in the endpoint.
    #  JSON keys:   streamType, directory
    #
    #######################################################################
    def getEndpointStreams(self):

        streams = []
        for str in range(len(self.endpoint['streams'])):
            strm = self.endpoint['streams'][str]
            streams.append({'streamType':strm['streamType'], 'directory':strm['directory']})

        return streams


    #######################################################################
    #
    #  Method: createEndpoint
    #
    #  Utility function to create the endpoint
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def createEndpoint(self, epJSON):
        status = -1

        ##  Request to create the endpoint

        endpointURL = 'https://'+self.ip+'/api/service/dig/config/endpoint'
        ep = requests.post(endpointURL, cookies=self.cookie, headers=self.auth, verify=False, json=epJSON)
        if ep.status_code == 200:
            status = 0

        return status

    #######################################################################
    #
    #  Method: deleteEndpoint
    #
    #  Utility function to delete the endpoint
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def deleteEndpoint(self, deviceName):
        status = -1

        ##  Request to delete the endpoint

        endpointURL = 'https://'+self.ip+'/api/service/dig/config/endpoint/'+deviceName
        ep = requests.delete(endpointURL, cookies=self.cookie, headers=self.auth, verify=False)
        if ep.status_code == 200:
            status = 0

        return status

    #######################################################################
    #
    #  Method: updatePassword
    #
    #  Utility function to set the device's channel password
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def updatePassword(self, chlType, newPassword):
        status = -1

        ##  Find the channel
        for ch in range(len(self.endpoint['channels'])):
            if  self.endpoint['channels'][ch]['type'] == chlType:
                self.endpoint['channels'][ch]['password'] = newPassword

        ##  Request to update the endpoint

        endpointURL = 'https://'+self.ip+'/api/service/dig/config/endpoint'
        ep = requests.put(endpointURL, cookies=self.cookie, headers=self.auth, verify=False, json=self.endpoint)
        if ep.status_code == 200:
            status = 0

        return status

    #######################################################################
    #
    #  Method: updateSFTPpassword
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def updateSFTPpassword(self, newPassword):

        status = self.updatePassword('CHANNEL_TYPE_SFTP', newPassword)
        if status == 0:
            self.SFTPpwd = newPassword

        return status


    #######################################################################
    #
    #  Method: updateRESTpassword
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def updateRESTpassword(self, newPassword):

        status = self.updatePassword('CHANNEL_TYPE_REST', newPassword)
        if status == 0:
            self.RESTpwd = newPassword

        return status

    #######################################################################
    #
    #  Method: testSFTPendpoint
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def testSFTPendpoint(self):

        ##  Find the SFTP channel to get the user/password

        for c in self.endpoint['channels']:
            if c['type'] == 'CHANNEL_TYPE_SFTP':

                pingResp = os.system("ping -c 1 -W 2 " + c['remoteHost'] + " >/dev/null")
                if pingResp != 0:
                    print("\tSBC {} is not reachable".format(c['remoteHost']))
                    return -1, 0

                payload={'remoteHost':c['remoteHost'], 'port':c['port'], 'authType':c['authType'], 'userId':c['userId'], 'password':"" if c['authType'] == 'AUTH_TYPE_SSH_KEY' else self.getSFTPpassword() }
                print('\tHost: {}, User: {}, {}: {}'.format(c['remoteHost'], c['userId'], 'SSH Key' if c['authType'] == 'AUTH_TYPE_SSH_KEY' else 'Pwd', self.getSSHkey() if c['authType'] == 'AUTH_TYPE_SSH_KEY' else self.getSFTPpassword() ))
                endpointURL = 'https://'+self.ip+'/api/service/dig/gendig/connection/sftp'

                ep = requests.post(endpointURL, cookies=self.cookie, headers=self.auth, verify=False, json=payload)

        return 0 if ep.status_code == 200 else -1, ep.status_code

    def getAuth(self, chl):
        if chl['authType'] == 'AUTH_TYPE_USERNAME_PASSWORD':
            if chl['type'] ==  'CHANNEL_TYPE_REST':
                return self.getRESTpassword()
            else:
                return self.getSFTPpassword()
        else:
            return self.sshKey
    
    #######################################################################
    #
    #  Method: printEndpoint
    #
    #  Returns:  0 if successful
    #            -1 if failed
    #
    #######################################################################
    def printEndpoint(self):
        channels = self.endpoint['channels']
        streams = self.endpoint['streams']
        ep = self.endpoint

        print("\nEndpoint {}: epType {}, timezone {}".format(ep['name'], ep['epType'], ep['timezone']))
        print("\tChannels:")

        ##  Print the password/ssh key per channel.
        for ch in range(len(channels)):
            print("\t\t{}: type: {}, remoteHost: {}, authType: {}, user: {}, password: {}".\
                  format(ch, channels[ch]['type'], channels[ch]['remoteHost'], channels[ch]['authType'], channels[ch]['userId'], self.getAuth(channels[ch])))
        print("\tStreams:")
        for str in range(len(streams)):
            print("\t\t{}: type: {}, directory: {}".format(str, streams[str]['streamType'], streams[str]['directory']))


