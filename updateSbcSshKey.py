#!/usr/bin/env python3

###################################################################################
#
#   updateSbcSshKey.py
#
#   Update the SSH key for the specified user.
#
###################################################################################

import argparse
import requests
import json
import subprocess
import urllib3
import base64
import syslog
import os
import sys

import pexpect
from pexpect import *
from pexpect import pxssh, TIMEOUT
from pathlib import Path

import pdb 

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

pgmName = Path(sys.argv[0]).stem

########################################################
#
#  Method: connectToRemote
#
#  Create connection to the remote.
#
#  Returns the pxssh handle
#
########################################################
def connectToRemote(sbcuser, sbcpassword, sbcip):

    status = -1
    conn = None
    try:

        ##  Connect to the SBC
        conn = pxssh.pxssh(options={'port':'2024'})
        status = conn.login(sbcip, sbcuser, sbcpassword)

    except pxssh.ExceptionPxssh as e:
        print("\tSBC: {}, Failed to login as user '{}', ({}).".format(sbcip, sbcuser, e))
    except pexpect.exceptions.EOF:
        print("\tSBC: {}, Timeout when logging in as {}. {}".format(sbcip, sbcuser, conn.before.decode('utf-8').strip()))

    return status, conn

########################################################
#
#  Method: executeCmdOnRemote
#
#  Execute the command on the remote server.
#  conn is the handle.
#
########################################################
def executeCmdOnRemote(conn, command):

    cmdStatus = -1
    cmdOutput = ""
    try:
        status = conn.sendline(command)
        status = conn.prompt()             # match the prompt
        cmdOutput = conn.before.decode('utf-8').splitlines()
        cmdStatus = cmdOutput[len(cmdOutput)-1]
    except pxssh.ExceptionPxssh as e:
        cmdOutput = "Failed to execute command '{}', ({}).".format(command, e)

    return cmdStatus, cmdOutput


########################################################
#
#  Method: checkForAuthorizedKeys
#
#  Check whether the authorized_keys exists
#
########################################################
def checkForAuthorizedKeys(sbcuser, sbcpassword, ip):

    sshDir = "~{}/.ssh".format(sbcuser)
    authorizedKeysFile = "{}/authorized_keys".format(sshDir)

    status, conn = connectToRemote(sbcuser, sbcpassword, ip)
    if conn == None  or status == -1:
        return

    cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'hostname; echo $?')
    if cmdStatus != '0':
        print("\tSBC IP: {}, Failed to get hostname ()".format(ip, cmdOutput[len(cmdOutput)-2]))
    else:
        hostInfo = "{} ({})".format(ip, cmdOutput[len(cmdOutput)-2])

    cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'ls -l {}; echo $?'.format(sshDir))
    if cmdStatus != '0':
        print("\tSBC: {}, {} DOES NOT exist".format(hostInfo, sshDir))
        return

    cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'ls -l {}; echo $?'.format(authorizedKeysFile))
    if cmdStatus != '0':
        print("\tSBC: {}, {} DOES NOT exist".format(hostInfo, authorizedKeysFile))
        return
    else:
        print("\tSBC: {}, {} exists".format(hostInfo, authorizedKeysFile))

    return

########################################################
#
#  Method: updateSshKeyForSBC
#
#  Login to SBC to update the authorized_keys file with
#  an SSH key.
#
#  1. Create the .ssh directory if it does not exist
#     with permission of 750
#  2. Create the authorized_keys file if it does not exist
#     with permission of 644
#
########################################################
def updateSshKeyForSBC(sbcuser, sbcpassword, sbcip, sshKey):

    status = -1

    ## Ensure that the SBC is reachable.

    pingResp = os.system("ping -c 1 -W 2 " + sbcip + " >/dev/null")
    if pingResp != 0:
        print("SBC IP: {}, ping failed".format(sbcip))
        return status

    print("SBC IP: {}".format(sbcip))

    ##  Connect to the SBC
    status, conn = connectToRemote(sbcuser, sbcpassword, sbcip)
    if conn == None  or status == -1:
        return status

    ##  Get the hostname
    ##  Create the .ssh directory, if necessary
    ##  Create the authorized_keys file, if necessary
    ##  Write the ssh key to the authorized_keys file

    try:

        sshDir = "~{}/.ssh".format(sbcuser)
        authorizedKeysFile = "{}/authorized_keys".format(sshDir)

        ##  Get hostname (for informational purposes only)
        cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'hostname; echo $?')
        if cmdStatus != '0':
            print("\tSBC IP: {}, Failed to get hostname ()".format(sbcip, cmdOutput[len(cmdOutput)-2]))
            conn.logout()
            return status
        else:
            hostInfo = "{} ({})".format(sbcip, cmdOutput[len(cmdOutput)-2])

        ##  Check if the .ssh directory exists.  Create it if necessary.
        cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'ls -l {}; echo $?'.format(sshDir))
        if cmdStatus != '0':
            print("\tSBC: {}, {} does not exist, creating it ....".format(hostInfo, sshDir))
            status = conn.sendline('mkdir {}; chmod 750 {}; echo $?'.format(sshDir, sshDir))
            status = conn.prompt()             # match the prompt
            cmdOutput = conn.before.decode('utf-8').splitlines()
            cmdStatus = cmdOutput[len(cmdOutput)-1]
            if cmdStatus != '0':
                print("\tSBC: {}, Failed to create {} ({})".format(hostInfo, sshDir, cmdOutput[len(cmdOutput)-2]))
                conn.logout()
                return status
            else:
                print("\tSBC: {}, Created {}".format(hostInfo, sshDir))

        ##  Check if the authorized_keys file exists. Create it if necessary.

        cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'ls -l {}; echo $?'.format(authorizedKeysFile))
        if cmdStatus != '0':
            print("\tSBC: {}, {} does not exist, creating it ....".format(hostInfo, authorizedKeysFile))
            status = conn.sendline('touch {}; chmod 644 {}; echo $?'.format(authorizedKeysFile, authorizedKeysFile))
            status = conn.prompt()             # match the prompt
            cmdOutput = conn.before.decode('utf-8').splitlines()
            cmdStatus = cmdOutput[len(cmdOutput)-1]
            if cmdStatus != '0':
                print("\tSBC: {}, Failed to create {} ({})".format(hostInfo, authorizedKeysFile, cmdOutput[len(cmdOutput)-2]))
                conn.logout()
                return status
            else:
                print("\tSBC: {}, Created {}".format(hostInfo, authorizedKeysFile))

        ##  If the SSH key already exists in the authorized_keys file, don't add it again

        cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'grep -Fx "{}" {}; echo $?'.format(sshKey, authorizedKeysFile))
        if cmdStatus == '0':
            print("\tSBC: {}, SSH key already exists, not added to {}".format(hostInfo, authorizedKeysFile))

        ##  Key does not exist, add it.
        else:
            cmdStatus, cmdOutput = executeCmdOnRemote(conn, 'echo "{}" >> {}; echo $?'.format(sshKey, authorizedKeysFile))
            if cmdStatus != '0':
                print("\tSBC: {}, Failed add SSH key to {} ({})".format(hostInfo, authorizedKeysFile, cmdOutput[len(cmdOutput)-1]))
            else:
                print("\tSBC: {}, Added SSH key to {}".format(hostInfo, authorizedKeysFile))

        conn.logout()
        
    except pxssh.ExceptionPxssh as e:
        print("\tSBC: {}, Failed to login as user '{}', ({}).".format(sbcip, sbcuser, e))
        status = -1
    except pexpect.exceptions.EOF:
        print("\tSBC: {}, Timeout when logging in as {}. {}".format(sbcip, sbcuser, conn.before.decode('utf-8').strip()))
        status = -1

    return status

########################################################
#
#  Method: main
#
########################################################
def main():

    pgmName = Path(sys.argv[0]).stem

    #  Setup to parse the command line arguments

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    required= parser.add_argument_group(title='Required arguments')
    optional= parser.add_argument_group(title='Optional arguments')

    optional.add_argument('-d',  action='store_true', default=False, dest='bDebug', help='Enable debug logging (optional)')
    optional.add_argument('-c',  action='store_true', dest='bCheckKeys', default=False, help="Check whether authorized_keys file exists.")
    optional.add_argument('-k',  action='store', dest='sshKeyFile', default='', required=False, help='File that contains the SSH key (from Analytics)')

    required.add_argument('-u',  action='store', dest='sbcuser', required=True, help='SBC user ID')
    required.add_argument('-p',  action='store', dest='sbcpassword', required=True, help='SBC user password')
    required.add_argument('-f',  action='store', dest='sbcsFile',  required=True, help="Contains the list of SBC IPs, one per line.")

    #  Print help if no arguments

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    #  Parse the arguments

    args = parser.parse_args()    #  Get the arguments

    if args.bDebug == True:
        pdb.set_trace()
        
    ##  If just checking for the authorized_keys file, don't need the ssh key file
    if args.bCheckKeys == False:
        if len(args.sshKeyFile) == 0:
            print("Must specify the sskKeyFile (-k)")
            sys.exit(1)

        ##  Check that ssh key file exists
        else:
            sshKey = ""
            try:
                with open(args.sshKeyFile, 'r') as file:
                    sshKey = file.read().rstrip()
            except OSError:
                print("Could not open/read the SSH key file: {}".format(sshKeyFile))
                sys.exit(0)

    ##  Read the SBC IPs from the file.

    try:
        fhSBCs=open(args.sbcsFile, 'r')
        sbcsIPList = [line.rstrip() for line in fhSBCs.readlines()]
        fhSBCs.close()              
    except Exception as e:
        print("Failed to open file {}, error={}".format(args.sbcsFile, e))
        sys.exit(1)

    ##  For each SBC

    for ip in sbcsIPList:
        if args.bCheckKeys == True:
            checkForAuthorizedKeys(args.sbcuser, args.sbcpassword, ip)
        else:
            updateSshKeyForSBC(args.sbcuser, args.sbcpassword, ip, sshKey)
    
    sys.exit(0)

if __name__ == '__main__':
    main()

