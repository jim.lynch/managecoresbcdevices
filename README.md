

## manageCoreSBCDevices
Choose a self-explaining name for your project.

## Description
   Manage Protect SBC Core devices by doing one of the following:
     1.  Save all devices to file in JSON format. (to be used when updating)
     2.  Update devices to remove the REST channel and set the authorization type for the SFTP channel to AUTH_TYPE_SSH_KEY.
     3.  Test device connections.
     4.  Read the JSON that was created with the 'save' action.

    Main purpose of script is to bulk update the Analytics Core SBC devices to remove the REST channel and update the SFTP channel to use the SSH key.


## Installation

Copy the three files Protect_manageSBCDevices.py, Protect_MagUser.py and Protect_Endpoint.py to any directory on the RA node, preferrably under vigiladmin's home directory (/home/vigiladmin).

## Usage

To get the following usage, run the Protect_manageSBCDevices.py script without arguments (./Protect_manageSBCDevices).

usage: Protect_manageSBCDevices.py [-h] [-c] [-d] [-x] [-filter DEVICESFILTER]
                                   [-f DEVICESFILE] -u MAGUSER -p MAGPASSWORD
                                   -a {save,update,test,read}

optional arguments:
  -h, --help            show this help message and exit

Required arguments:
  -u MAGUSER            MAG user ID (default: None)
  -p MAGPASSWORD        MAG user password (default: None)
  -a {save,update,test,read}
                        Action to be performed (save, update, test) (default:
                        None)

Optional arguments:<br>
  -c                    Test connection to device after updating (only
                        pertains to the "update" action) (default: False)<br>
  -d                    Enable debug logging (optional) (default: False)<br>
  -x                    Exclude the REST channel (for "save" only) (default:False)<br>
  -filter DEVICESFILTER
                        Text file that contains list of devices to be updated, one device per line. If no filter file is specified,
all devices in the devicesFile will be updated.<br>
                        (default: )<br>
  -f DEVICESFILE        For the 'update' action, this file contains the JSON that was previously created when the 'save' action was executed. For the 'save' action, all the SBC Core<br>
devices will be retrieved and written in JSON format to the file. For the 'read' action, this file should be the name of a file created with the 'save' action. (default: None)<br>

**To SAVE the device configurations to a JSON file**
    Change diretory to location of the Protect_manageSBCDevices.py script and run:

    ./Protect_manageSBCDevices.py -u RA-user -p RA-password -a save -f PROD.json

    This will save the configuration of all devices to the PROD.json file.

**To UPDATE the device configurations from a JSON file**

    The 'update' action will use the JSON file saved with the 'save' action.  It will do the following:
        1. Delete the SBC device
        2. Modify the configuration to remove the REST channel, if it exists.
        3. Change authorization type to AUTH_TYPE_SSH_KEY
        4. Create the device.  

    The 'update' action can optionally take a filter file (-filter) which will only update those devices. This file contains one SBC name per line.

**To READ the device configurations from a JSON file**

    ./Protect_manageSBCDevices.py -u RA-user -p RA-password -a read -f PROD.json

    This will print the device name and the channel type, IP, authorization type and user ID for each channel.

**To TEST the SFTP connection to each device**

    ./Protect_manageSBCDevices.py -u RA-user -p RA-password -a read -f devices.list

    This will attempt to connect to the SFTP channel each SBC listed in the devices.list file.  The devices.list file 
    will contain the SBC name, one per line. 


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
