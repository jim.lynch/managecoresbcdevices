#!/usr/bin/env python3

###################################################################################
#
#   Protect_manageSBCDevices.py
#
#   Manage Protect SBC Core devices by doing one of the following:
#     1.  Save all devices to file in JSON format. (to be used when updating)
#     2.  Update devices to remove the REST channel.
#     3.  Test the connections to the list of devices.
#
#   Currently, this script is a one-off since the REST channel is configured for
#   all SBC devices but is not required.  
#
###################################################################################

import argparse
import requests
import json
import subprocess
import urllib3
import base64
import syslog
import os
import sys

from pathlib import Path

from Protect_MagUser import Protect_MagUser
from Protect_Endpoint import Protect_Endpoint

import pdb 

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

pgmName = Path(sys.argv[0]).stem

########################################################
#
#  Method: getAllSBCCoreDevices
#
#  Returns a list of all the SBC Core device names
#
########################################################
def getAllSBCCoreDevices(eps):
    deviceList = []

    for ep in eps:
        deviceList.append(ep['name'])

    return deviceList


#############################################################
#
#  Method: buildFilteredList
#
#  Filter the list of devices and return the filtered list.
#
#  Get all the devices and then filter them using the
#  device names in the filter file, if specified.
#
#  filterFile - list of SBC device names, one per line
#  eps - All the Protect devices.
#
#############################################################
def buildFilteredList(filterFile, eps):
    status = 0

    ##  If filter file specified, read the devices into the device list

    deviceList = []
    if len(filterFile):
        try:
            with open(filterFile) as file:
                deviceList = [line.rstrip() for line in file]

        except Exception as e:
            print("Failed to open file {}, error={}".format(filterFile, e))
            status = -1

    ##  No device filter specified, return the list of all devices
    else:
        deviceList = getAllSBCCoreDevices(eps)

    return status, deviceList


########################################################
#
#  Method: main
#
########################################################
def main():

    sbcEps = []

    pgmName = Path(sys.argv[0]).stem

    #  Setup to parse the command line arguments

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    required= parser.add_argument_group(title='Required arguments')
    optional= parser.add_argument_group(title='Optional arguments')

    optional.add_argument('-c',  action='store_true', default=False, dest='bConnectionTest', help='Test connection to device after updating (only pertains to the "update" action)')
    optional.add_argument('-d',  action='store_true', default=False, dest='bDebug', help='Enable debug logging (optional)')
    optional.add_argument('-x',  action='store_true', default=False, dest='bExcludeREST', help='Exclude the REST channel (for "save" only)')

    optional.add_argument('-filter',  action='store', default='', dest='devicesFilter',
                          help=
                          "Text file that contains list of devices to be updated, one device per line."
                          "If no filter file is specified, all devices in the devicesFile will be updated."
                          )
    optional.add_argument('-f',  action='store', dest='devicesFile',  
                          help=
                          "For the 'update' action, this file contains the JSON that was previously created when the 'save' action was executed."
                          "For the 'save' action, all the SBC Core devices will be retrieved and written in JSON format to the file."
                          )
    optional.add_argument('-pu',  action='store', default='linuxadmin', dest='sftpUser', help="The SFTP channel's user name")

    required.add_argument('-u',  action='store', dest='maguser', required=True, help='MAG user ID')
    required.add_argument('-p',  action='store', dest='magpassword', required=True, help='MAG user password')
    required.add_argument('-a',  action='store', dest='action', required=True, choices=['list', 'save', 'update', 'test'], help='Action to be performed')

    #  Print help if no arguments

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    #  Parse the arguments

    args = parser.parse_args()    #  Get the arguments

    if args.bDebug == True:
        pdb.set_trace()

    ##  Make sure a file is specified for the 'save' or 'update' options

    if args.action == 'save'  or args.action == 'update':
        if args.devicesFile == None:
            print("The '{}' action requires the -f option.".format(args.action))
            sys.exit(1)

    ##  Instantiate the Mag object and Login

    magUser = Protect_MagUser(args.maguser, args.magpassword)

    status, errMsg = magUser.login()
    if status != 200:
        print("{}: Failed to login as user {}, error={}".format(pgmName, args.maguser, status))
        sys.exit(1)

    ##  Instantiate the endpoint object

    endpoint = Protect_Endpoint(*magUser.getCredentials())

    ##  Get all the devices

    status, allEps = endpoint.getAllEndpoints()
    if status != 200:
        print('Failed to get the endpoints, status is {}'.format(status))
        magUser.logout()
        sys.exit(1)

    ##  Now filter the SBC Core devices

    eps = []
    for e in allEps:
        if e['epType'] == 'ENDPOINT_TYPE_SBC':
            eps.append(e)

    ##  If saving devices ....
    if args.action == 'save':

        ##  File where JSON is to be written
        try:
            fhEps=open(args.devicesFile, 'w')
        except Exception as e:
            print("Failed to open file {}, error={}".format(args.devicesFile, e))
            magUser.logout()
            sys.exit(1)

        ##  Get the filtered list of devices.

        deviceList = []
        status, deviceList = buildFilteredList(args.devicesFilter, eps)
        if status == -1:
            magUser.logout()
            sys.exit(1)

        ##  For each device, remove the REST channel if it exists
        for ep in eps:

            if ep['name'] not in deviceList:
                print("Device: {}, Skipping".format(ep['name']))
                continue

            ## If only saving the SFTP channel ....

            if args.bExcludeREST:
                ##  If there is a REST channel, remvove it
                chls = ep['channels']
                for c in range(len(chls)):
                    if chls[c]['type'] == 'CHANNEL_TYPE_REST':
                        chls.pop(c)
                        break

            ep['channels'][0]['userId'] = args.sftpUser
            print("Device: {} ({}) saved.".format(ep['name'], ep['channels'][0]['remoteHost']))
            sbcEps.append(ep)

        ##  Save the devices

        try:
            json.dump(sbcEps, fhEps)
            print("Saved the devices in JSON format to file {}".format(args.devicesFile))
            fhEps.close()
        except Exception as e:
            print("Failed to save the devices JSON to file {}, error={}".format(args.devicesFile, e))

    ##  If updating devices...
    ##      1. Build the filtered list of devices (should be a subset of devices in the JSON file)
    ##      2. Load the devices in JSON format from the file created with the 'save' option.
    ##      3. For each device in the filtered list:
    ##          a. Remove the REST channel
    ##          b. Set the authType to AUTH_TYPE_SSH_KEY
    elif args.action == 'update':

        ##  Get the filtered list of devices.

        deviceList = []
        status, deviceList = buildFilteredList(args.devicesFilter, eps)
        if status == -1:
            magUser.logout()
            sys.exit(1)

        try:
            fhEps=open(args.devicesFile, 'r')
            epsRaw = json.load(fhEps)

            ##  If only one SBC in the file, convert it to a list of 1

            eps = []
            if type(epsRaw) is dict:
                eps.append(epsRaw.copy())
            else:
                eps = epsRaw

            fhEps.close()
        except Exception as e:
            print("Failed to open file {}, error={}".format(args.devicesFile, e))
            magUser.logout()
            sys.exit(1)    

        ##  If the deviceList is empty, no filter file was specified
        ##  so load all the devices into the list.

        if len(deviceList) == 0:
            deviceList = getAllSBCCoreDevices(eps)

        ##  For all the Core SBCs

        for ep in eps:

            ##  Make sure device exists
            status, theEP = endpoint.getEndpoint(ep['name'])
            if status != 0:
                print("Device: {} does not exist".format(ep['name']))
                continue

            ##  If device not in the list, skip it
            if ep['name'] not in deviceList:
                print("Device: {}, Skipping".format(ep['name']))
                continue
            deviceList.remove(ep['name'])
            
            ##  Iterate through device channels.
            ##  If there is a REST channel, remvove it.
            ##  Else if it's the SFTP channel, set the authType to use an SSH key
            ##  NOTE:  SFTP channel should be the first in the list of two channels
            ##         but just in case that is not always the case, we'll loop through the 
            ##         channels until the REST is found and remove it from the list.
            ##         
            chls = ep['channels']
            for c in range(len(chls)):
                if chls[c]['type'] == 'CHANNEL_TYPE_REST':
                    print("Device: {}, removing the REST channel".format(ep['name']))
                    chls.pop(c)
                    break

            ##  Now set the authorization type to SSH Key.
            ##  Don't set the password, Analytics will assign it.

            for c in range(len(chls)):
                if chls[c]['type'] == 'CHANNEL_TYPE_SFTP':
                    chls[c]['authType'] = 'AUTH_TYPE_SSH_KEY'

            ##  Need to delete the device before creating it

            status = endpoint.deleteEndpoint(ep['name'])
            if status == 0:
                print('Device: {}, successfully deleted'.format(ep['name']))

                ##  Create the updated device

                status = endpoint.createEndpoint(ep)
                if status == 0:
                    print('Device: {}, successfully created'.format(ep['name']))

                    ##  Test the connection to the updated device

                    if args.bConnectionTest:
                        print('Device: {}, testing connection'.format(ep['name']))
                        status, http_status = endpoint.testSFTPendpoint()
                        if status == 0:
                            print('Device: {}, successfully connected'.format(ep['name']))
                        else:
                            print('Device: {}, connection failed, status: {}'.format(ep['name'], http_status))
                else:
                    print('Device: {}, failed to create'.format(ep['name']))
            else:
                print("Device: {}, delete failed, therefore not created".format(ep['name']))

        if len(deviceList):
            print("Devices that were not found: {}".format(', '.join(deviceList)))

    ##  If testing connection to devices...

    elif args.action == 'test':

        ##  Get the filtered list of devices.

        deviceList = []
        status, deviceList = buildFilteredList(args.devicesFilter, eps)
        if status == -1:
            magUser.logout()
            sys.exit(1)

        for ep in eps:
            status, theEP = endpoint.getEndpoint(ep['name'])
            if status != 0:
                print("Device: {} does not exist".format(ep['name']))
                continue

            if ep['name'] not in deviceList:
                print("Device: {}, Skipping".format(ep['name']))
                continue
            deviceList.remove(ep['name'])

            print("Device: {} Testing connection".format(ep['name']))
            status, http_status = endpoint.testSFTPendpoint()
            print("\tDevice: {} connection status {}".format(ep['name'], status))

    ##  If listing the IPs of all devices...

    elif args.action == 'list':

        ##  Get the filtered list of devices.

        deviceList = []
        status, deviceList = buildFilteredList(args.devicesFilter, eps)
        if status == -1:
            magUser.logout()
            sys.exit(1)

        for ep in eps:
            status, theEP = endpoint.getEndpoint(ep['name'])
            if status != 0:
                print("Device: {} does not exist".format(ep['name']))
                continue

            if ep['name'] not in deviceList:
                print("Device: {}, Skipping".format(ep['name']))
                continue
            endpoint.printEndpoint()

#            print("Device: {}, IP: {}".format(ep['name'], ep['channels'][0]['remoteHost']))

    magUser.logout()    
    
    sys.exit(0)

if __name__ == '__main__':
    main()

